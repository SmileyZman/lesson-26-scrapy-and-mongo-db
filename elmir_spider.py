from scrapy import Spider


class ElmirCategorySpider(Spider):
    name = 'elmir'
    start_urls = ["https://elmir.ua/cell_phones/", ]

    def parse(self, response, **kwargs):
        # print(dir(response))
        # item_names = response.css('#vitrina-tovars')
        # item_names = response.xpath('/html/body/div[1]/main/article/div[1]/div/div[1]/div/div[7]/ul/li[1]/section/a[1]')
        item_names = response.css('li.vit-item:nth-child(4) > section:nth-child(2) > a:nth-child(1)')
        for name in item_names:
            print(name)


# li.vit-item:nth-child(2) > section:nth-child(2) > a:nth-child(1)
# div.card:nth-child(2) > h2:nth-child(2) > a:nth-child(1)